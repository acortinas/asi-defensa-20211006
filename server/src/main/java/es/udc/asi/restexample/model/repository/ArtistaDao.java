package es.udc.asi.restexample.model.repository;

import java.util.List;

import es.udc.asi.restexample.model.domain.Artista;

public interface ArtistaDao {

  void create(Artista artista);

  List<Artista> findAll();

  Artista findById(Long id);
}
