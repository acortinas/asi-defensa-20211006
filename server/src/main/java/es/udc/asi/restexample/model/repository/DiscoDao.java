package es.udc.asi.restexample.model.repository;

import java.util.List;

import es.udc.asi.restexample.model.domain.Disco;

public interface DiscoDao {
  List<Disco> findAll();

  void create(Disco disco);

  List<Disco> findByArtista(Long id);

  Disco findById(Long id);

  void update(Disco disco);
}
