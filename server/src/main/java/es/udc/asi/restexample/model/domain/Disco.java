package es.udc.asi.restexample.model.domain;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.TableGenerator;

@Entity
@TableGenerator(name = "album_generator", initialValue = 234552, allocationSize = 50)
public class Disco {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "album_generator")
  private Long id;

  private String titulo;

  @ManyToOne(fetch = FetchType.EAGER)
  private Artista interprete;

  @OneToMany(mappedBy = "disco", fetch = FetchType.EAGER)
  private Set<Cancion> canciones = new HashSet<>();

  public Disco() {
  }

  public Disco(String titulo, Artista interprete, String... canciones) {
    this.setInterprete(interprete);
    this.setTitulo(titulo);
    for (int i = 0; i < canciones.length; i++) {
      this.getCanciones().add(new Cancion(i + 1, canciones[i], this));
    }
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public Artista getInterprete() {
    return interprete;
  }

  public void setInterprete(Artista interprete) {
    this.interprete = interprete;
  }

  public Set<Cancion> getCanciones() {
    return canciones;
  }

  public void setCanciones(Set<Cancion> canciones) {
    this.canciones = canciones;
  }
}
