package es.udc.asi.restexample.model.repository;

import es.udc.asi.restexample.model.domain.Cancion;

public interface CancionDao {

  void create(Cancion cancion);
}
