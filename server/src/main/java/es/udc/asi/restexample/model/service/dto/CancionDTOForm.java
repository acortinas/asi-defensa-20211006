package es.udc.asi.restexample.model.service.dto;

public class CancionDTOForm {
  private String titulo;

  public CancionDTOForm() {
  }

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

}
