package es.udc.asi.restexample.model.domain;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.TableGenerator;

@Entity
@TableGenerator(name = "cancion_generator", initialValue = 6662333)
public class Cancion {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "cancion_generator")
  private Long id;

  private Integer numeroDePista;

  private String titulo;

  @ManyToOne(fetch = FetchType.LAZY)
  private Disco disco;

  public Cancion() {
  }

  public Cancion(int numPista, String titulo, Disco album) {
    this.setTitulo(titulo);
    this.setNumeroDePista(numPista);
    this.setDisco(album);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public Integer getNumeroDePista() {
    return numeroDePista;
  }

  public void setNumeroDePista(Integer numeroDePista) {
    this.numeroDePista = numeroDePista;
  }

  public String getTitulo() {
    return titulo;
  }

  public void setTitulo(String titulo) {
    this.titulo = titulo;
  }

  public Disco getDisco() {
    return disco;
  }

  public void setDisco(Disco disco) {
    this.disco = disco;
  }
}
