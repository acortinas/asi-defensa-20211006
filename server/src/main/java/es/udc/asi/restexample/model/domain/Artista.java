package es.udc.asi.restexample.model.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.TableGenerator;

@Entity
@TableGenerator(name = "artista_generator", initialValue = 4356, allocationSize = 50)
public class Artista {

  @Id
  @GeneratedValue(strategy = GenerationType.TABLE, generator = "artista_generator")
  private Long id;

  private String nombre;

  public Artista() {
  }

  public Artista(String nombre) {
    this.setNombre(nombre);
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getNombre() {
    return nombre;
  }

  public void setNombre(String nombre) {
    this.nombre = nombre;
  }
}
