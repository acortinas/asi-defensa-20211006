import axios from "axios";

const HTTP = axios.create({
  baseURL: "http://localhost:8080/api/",
  // baseURL: "http://alexcortinas.es/asi/api/",
});

export default HTTP;
